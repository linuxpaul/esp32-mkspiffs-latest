#include "freertos/FreeRTOS.h"
#include "sdkconfig.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
//#include "spiffs_vfs.h"
//#include "esp_vfs.h"
//#include "esp_vfs_fat.h"
#include "esp_spiffs.h"
#include "driver/gpio.h"
#include <sys/fcntl.h>
#include "string.h"
#include "mongoose.h"

struct device_settings {
  char val[100];
 };

static struct mg_serve_http_opts s_opts;
static struct device_settings s_button = {"value1"};

static const char *TAG = "example";

static void mongoose_event_handler(struct mg_connection *nc, int ev, void *evData)
{
	struct http_message *hm = (struct http_message *) evData;
	if (ev == MG_EV_HTTP_REQUEST) {
		
/*	      if (mg_vcmp(&hm->uri, "/button") == 0) {
	    	  mg_get_http_var(&hm->body, "action", s_button.val, sizeof(s_button.val));
	    	  char* mimeType = "text/plain";
	    	  int contentLength = strlen(s_button.val);


//	    	  mg_printf_html_escape(nc, "%s", s_button.val);
//	    	  mg_http_send_redirect(nc, 302, mg_mk_str("/"), mg_mk_str(NULL));
	      } else {
*/		printf("request received\n");
	    mg_serve_http(nc, hm, s_opts);
//	      }
	}
}

void mongooseTask(void *data)
{
	struct mg_mgr mgr;
	mg_mgr_init(&mgr, NULL);
	struct mg_connection *nc = mg_bind(&mgr, ":80", mongoose_event_handler);
	if (nc == NULL) {
		printf( "No connection from the mg_bind()\n");
		vTaskDelete(NULL);
		return;
	}

	mg_set_protocol_http_websocket(nc);
	memset(&s_opts, 0, sizeof(s_opts));
	s_opts.document_root= "/spiffs/";
	s_opts.index_files = "index.html";
	printf("mg goeas polling ... \n");
	while (1) {
		mg_mgr_poll(&mgr, 200);
	}
	mg_mgr_free(&mgr);
}

esp_err_t wifi_event_handler(void *ctx, system_event_t *event)
{
	switch (event->event_id){
		case SYSTEM_EVENT_AP_START:
			printf("Accesspoint connected\n");
			xTaskCreate(&mongooseTask, "mongooseTask", 20000, NULL, 5, NULL);
			break;

		case SYSTEM_EVENT_AP_STOP:
			printf("Accesspoint stopped\n");
			break;

		case SYSTEM_EVENT_AP_STACONNECTED:
			printf("Accesspoint Station connected\n");
			break;

		case SYSTEM_EVENT_AP_STADISCONNECTED:
			printf("Accesspoint Station disconnected\n");
			break;

		case SYSTEM_EVENT_AP_PROBEREQRECVED:
			printf("Accesspoint Probe Request received\n");
			break;

		default:
			break;
	}
	return ESP_OK;
}

static void init_wifi(void)
{
    tcpip_adapter_init();
    tcpip_adapter_ip_info_t info = { 0, };
    IP4_ADDR(&info.ip, 192, 168, 3, 1);
    IP4_ADDR(&info.gw, 192, 168, 3, 1);
    IP4_ADDR(&info.netmask, 255, 255, 255, 0);
    ESP_ERROR_CHECK(tcpip_adapter_dhcps_stop(TCPIP_ADAPTER_IF_AP));
    ESP_ERROR_CHECK(tcpip_adapter_set_ip_info(TCPIP_ADAPTER_IF_AP, &info));
    ESP_ERROR_CHECK(tcpip_adapter_dhcps_start(TCPIP_ADAPTER_IF_AP));

    ESP_ERROR_CHECK( esp_event_loop_init(wifi_event_handler, NULL) );
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_AP) );
    wifi_config_t ap_config = {
		.ap = {
			.ssid="ESP32",
			.ssid_len=0,
			.password="pwpwpwpw",
			.channel=0,
			.authmode=WIFI_AUTH_WPA2_PSK,
			.ssid_hidden=0,
			.max_connection=4,
			.beacon_interval=100
		}
    };
    ESP_ERROR_CHECK( esp_wifi_set_config(WIFI_IF_AP, &ap_config) );
    ESP_ERROR_CHECK( esp_wifi_start() );
}

void app_main(void)
{
    nvs_flash_init();

    ESP_LOGI(TAG, "Initializing SPIFFS");
    
    esp_vfs_spiffs_conf_t conf = {
      .base_path = "/spiffs",
      .partition_label = NULL,
      .max_files = 5,
      .format_if_mount_failed = true
    };
    
    // Use settings defined above to initialize and mount SPIFFS filesystem.
    // Note: esp_vfs_spiffs_register is an all-in-one convenience function.
    esp_err_t ret = esp_vfs_spiffs_register(&conf);

    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
        } else if (ret == ESP_ERR_NOT_FOUND) {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
        } else {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        return;
    }
    
    size_t total = 0, used = 0;
    ret = esp_spiffs_info(NULL, &total, &used);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
    } else {
        ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
    }

	
	DIR *dir = NULL;
	dir = opendir("/spiffs");
    if (dir) {
        printf("/spiffs exists\n");
		closedir(dir);
	} else {printf("Error opening directory\n");}

	FILE *fd = fopen("/spiffs/index.html", "rb");
	if (fd == NULL) {printf("fd ist NULL\n");}
    if (fd) {
        printf("index.html exists\n");
		fclose(fd);
	} else {printf("index.html NOT exists\n");}
	
   	init_wifi();

    gpio_set_direction(GPIO_NUM_2, GPIO_MODE_OUTPUT);
    int level = 0;
    while (true) {
        gpio_set_level(GPIO_NUM_2, level);
        level = !level;
        vTaskDelay(300 / portTICK_PERIOD_MS);
    }
}

